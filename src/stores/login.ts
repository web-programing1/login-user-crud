import { ref, computed } from "vue";
import { defineStore } from "pinia";
import { useUserStore } from "./user";
import { useMessageStore } from "./message";

export const useLoginStore = defineStore("counter", () => {
  const messageStore = useMessageStore();
  const userStore = useUserStore();
  const username = ref("");
  const isLogin = computed(() => {
    //username is not empty
    return username.value !== "";
  });

  const login = (userName: string, password: string): void => {
    if(userStore.login(userName, password)){
      username.value = userName;
      localStorage.setItem("username", userName);
    }else{
      messageStore.showMessage("Incorrect Username or Password");
    }
  };

  const logout = () => {
    username.value = "";
    localStorage.removeItem("username");
  };

  const loadData = () => {
    username.value = localStorage.getItem("username") || "";
  };

  return { username, isLogin, login, logout, loadData };
});
